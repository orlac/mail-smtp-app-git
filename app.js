var express = require('express'),
  config = require('./config/config'),
  db = require('./app/models');


var passport = require('passport');
var io = require('socket.io')();
var router = require('./app/events/router');
var app = express();

// var logger = require('./app/logger');
// logger.log(config);
// logger.logger. debug(config);

 
db.sequelize
  .sync()
  .then(function () {
  	
  	io.listen(config.socketPort);
    app.listen(config.port);
    //io.set('resource', config.socketResource);
    
    //io.serverClient(false);
    //io.set('authorization', app.user.auth);
    
    io.on('connection', function (client) {
        console.log('connection', client.id);
        router.start(client);
    });

  }).catch(function (e) {
    throw new Error(e);
  });



require("./config/passport")(passport, config, app);
require('./config/express')(app, config, passport);  

/*var bcrypt = require('bcryptjs');
bcrypt.hash('admin', 10, function(err, hash){
  console.log('admin', err, hash);
});*/