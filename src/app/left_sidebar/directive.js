'use strict';
(function(){
	
	var _fn = function(){
                return {
        		restrict: 'AE',
                        replace: true,
                	scope: {
                		logined: '@'
                	},
                	controller: function($scope, $element, $attrs){
                		$scope.isLogined = function(){
                			return $scope.logined > 0;
                		}

                                $scope.getClassState = function(_item, state){
                                        console.log('state', _item, state);
                                }
                	},
                	template: require('ng-cache!./template.html'),
                	// template: '<p>test</p>',
		};
	};

	angular.module('leftSidebar', ['ui.router'])
	.directive('leftSidebar',  _fn)	
})();
