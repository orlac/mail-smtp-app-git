'use strict';

var ctrl = function($scope, apiService, notifyService){
	apiService.ensureAccessNotLogin();
	this.data = {};
	this.submit = function(){
		apiService.userAuth({
			username: this.data.username,
			password: this.data.password,
		});
	}

};

angular.module('home', ['topSidebar', 'apiServiceModule', 'notifyServiceModule'])
	.controller('homeCtrl', [
		'$scope'
		,'apiService'
		,'notifyService'
		, ctrl
	]);