'use strict';

angular.module('notifyServiceModule', ['ui.bootstrap'])
	.factory('notifyService', ['$modal', '$sce', function($modal, $sce){

		var errorTpl=require('ng-cache!./errorTpl.html');
		var notifyTpl=require('ng-cache!./notifyTpl.html');
		var alertTpl=require('ng-cache!./alertTpl.html');

		var buildError = function(err){
			
			if( typeof err == 'object' ){

				err = err.responseJSON || err;
				
				var message = 'какая-то непонятная ошибка';
				if(err.message){
					message = err.message;
				}
				else if(err.statusText){
					message = err.statusText;	
				}
				else if(err.error){
					message = err.error;	
				}
				//var message = (err.message? err.message: (err.error? err.error: 'какая-то непонятная ошибка') ) ;
				var text = [];
				text.push( '<p>'+ message +'</p>');
				if(err.errors){
					for(var i in err.errors){
						text.push( '<p>'+ err.errors[i].message+'</p>');
					}
				}
				return text.join('\n');
				
			}else{
				var text = err || 'какая-то непонятная ошибка';
			}
			return text;
		}

		var service = {
			error: function(err){
				var modalInstance = $modal.open({
                    template: errorTpl,
                    controller: ['$scope', '$modalInstance', function($scope, $modalInstance){
                        $scope.title = 'Ошибка: ';
                        $scope.message = $sce.trustAsHtml(buildError(err));
                        $scope.modal = $modalInstance;
                    }]
                });
	  		}.bind(this),
	  		notify: function(text){
	  			var modalInstance = $modal.open({
                    template: notifyTpl,
                    controller: ['$scope', '$modalInstance', function($scope, $modalInstance){
                        $scope.message = $sce.trustAsHtml(text);
                        $scope.modal = $modalInstance;
                    }]
                });
	  		}.bind(this),
	  		alert: function(text){
	  			var modalInstance = $modal.open({
                    template: alertTpl,
                    controller: ['$scope', '$modalInstance', function($scope, $modalInstance){
                        $scope.message = $sce.trustAsHtml(text);
                        $scope.modal = $modalInstance;
                    }]
                });
	  		}.bind(this)
		};

		return service;

	}]);