'use strict';

angular.module('pageServiceModule', [])
	.factory('pageService', ['$window', function($window){
		
		    var publish = {

          setTitle: function(text){
            $window.document.title =  text;
          }
          
      	};

      	return publish;
	}]);