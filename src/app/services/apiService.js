'use strict';

angular.module('apiServiceModule', ['notifyServiceModule', 'ui.router'])
	.factory('apiService', ['$state', '$window', '$location', 'notifyService',/*'$state',*/ function($state, $window, $location, notifyService/*, $state*/){
		

		var publish = {

          ensureAccessNotLogin: function(){
            var me = this;
            this.getUser(function(){
                me.go('cabinet.index')
            }, function(){});  
          },
          ensureAccess: function(ignoreTimeZone){
            var me = this;
            var data = {};
            if(ignoreTimeZone){
              data.tz_i = true;
            }
            this._get_user(data, null, function(err){ 
                // me.go( err.go || '/')
                //todo resolve sref
                me.goout( err.go || '/')
                notifyService.error( err.message || 'Доступ запрещен' )
            });  
          },

          userAuth: function(data){
                var me = this;
                $.ajax({
                  dataType: 'json',
                  type: 'post',
                  url: '/auth/login',
                  data: data,
                  success: function(res){
                    console.log('/auth/login', res);
                    //me.goout( res.go || '/cabinet');
                    me.go('cabinetImport');
                  },
                  error: function(err){
                    console.log('/auth/login', err);
                    err = err.responseJSON || {};
                    notifyService.error( err.message || err.error || 'что-то пошло не так...' )
                  }
                });
          },

          _get_user: function(data, _ok, _err){
            var me = this;
            $.ajax({
              dataType: 'json',
              url: '/api/user/get',
              data: data,
              success: function(res){
                console.log('/api/user/get', res);
                if(_ok){
                  _ok(res);
                }
              },
              error: function(err){
                err = err.responseJSON || {};
                console.error('/api/user/get ', err);
                if(_err){
                  _err(err);
                }else{
                  notifyService.error( err.message || 'что-то пошло не так...' )
                }
              }
            });
          },

          getUser: function(_ok, _err){
            var me = this;
            me._get_user(null, _ok, _err);
          },

          goout: function(path){
            $window.location.href = path;
          },
          go: function(path){
            //todo костыльно это
            // scope.document.getElementById('router').go(path, {replace: true})
            // var href = $urlRouter.href( path );
            $state.go(path);
            // $window.location.href = path;
            // $location.path(path);
          }
      	};

      	return publish;
	}]);