var express = require('express');
var glob = require('glob');

var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
// var cookieSession = require('cookie-session');
var session = require('express-session');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');

module.exports = function(app, config, passport) {

  // app.use(favicon(config.root + '/public/img/favicon.ico'));

  app.set('views', config.root + '/app/views');
  app.set('view engine', 'ejs');
  
  app.use(logger('dev'));
  
  // app.use(bodyParser({ uploadDir: config.root+'/.tmp', keepExtensions: true }));
  app.use(bodyParser.urlencoded({
    extended: true,
    uploadDir: config.root+'/.tmp', keepExtensions: true
  }));
  app.use(bodyParser.json());
  // app.bodyParser({ uploadDir: 'photos' });
  
  
  app.use(cookieParser());
  // app.use(cookieSession({ secret: config.secret }));
  app.use(session({
    //resave: true,
    //saveUninitialized: true,
    secret: config.secret
  }));



  app.use(compress());
  app.use(express.static(config.root + '/public'));
  // app.use(express.static(config.root + '/files'));
  app.use(methodOverride());

   // use passport session
  app.use(passport.initialize());
  app.use(passport.session());


  var controllers = glob.sync(config.root + '/app/controllers/*.js');
  controllers.forEach(function (controller) {
    require(controller)(app);
  });

  //для спа страниц
  require(config.root + '/app/controllers/dummy.js')(app);

  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });
  
  //var errorTemplate = require('marko').load(require.resolve('../app/views/error.marko'));
  if(app.get('env') === 'development'){
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);

      res.render('error', {
        message: err.message,
        error: err,
        config: config,
        title: 'error',
        date: new Date().toUTCString()
      });
      // errorTemplate.render({
      //   message: err.message,
      //   error: err,
      //   title: 'error'
      // }, res);
    });
  }

  app.use(function (err, req, res, next) {
    console.log(err);
    res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        config: config,
        error: {},
        title: 'error',
        date: new Date().toUTCString()
      });
      // errorTemplate.render({
      //   message: err.message,
      //   error: {},
      //   title: 'error'
      // }, res);
  });

};
