//boot/passport.js
// var config = require("nconf");
//var passport = require('passport');
var AuthLocalStrategy = require('passport-local').Strategy;
var AuthFacebookStrategy = require('passport-facebook').Strategy;
var AuthVKStrategy = require('passport-vkontakte').Strategy;
 
var db = require('../app/models');

 
module.exports = function (passport, config, app) {


    passport.use('local', new AuthLocalStrategy(
        function (username, password, done) {
            
            db.user_auth.find({ 
                where: {
                    username: username
                } 
            }).then(function(_user) {
                if (!_user) { return done({ message: 'Unknown user ' + username }, false); }

                _user.comparePassword(password).then(function(){
                    _getUser(_user.user_id).then(function(user){
                        done(null, {
                            id: user.id,
                            uid: user.id,
                            type_auth: 'pw',
                            username: username,
                        });
                    }).catch(function(err){
                        done(err);
                    });
                }).catch(function(err){
                    return done(err);
                });

            }).catch(function(err){
                done(err);    
            });
        }
    ));

     
    passport.use('facebook', new AuthFacebookStrategy({
            clientID: config.passport.facebook.app_id,
            clientSecret: config.passport.facebook.secret,
            callbackURL: config.passport.facebook.url + "/auth/facebook/callback",
            passReqToCallback: true,
            // profileFields: [
            //     'id',
            //     'displayName',
            //     'profileUrl',
            //     "username",
            //     "link",
            //     "gender",
            //     "photos"
            // ]
        },
        function (req, accessToken, refreshToken, profile, done) {
            //console.log("facebook auth: ", profile);
            return done(null, {
                //если идет привязка логина к существующему
                loginedUser: req.user,
                accessToken: accessToken,
                refreshToken: refreshToken,
                type_auth: 'facebook',
                uid: profile.id,
                username: profile.displayName,
                //photoUrl: profile.photos[0].value,
                profileUrl: profile.profileUrl,
                profile: profile
            });
        }
    ));
     
    passport.use('vk', new AuthVKStrategy({
            clientID: config.passport.vk.app_id,
            clientSecret: config.passport.vk.secret,
            callbackURL: config.passport.vk.url + "/auth/vk/callback",
            passReqToCallback: true
        },
        function (req, accessToken, refreshToken, profile, done) {
            //  посмотреть в базе, записать ес че
            //console.log("vk auth: ", profile);
            return done(null, {
                //если идет привязка логина к существующему
                loginedUser: req.user,
                accessToken: accessToken,
                refreshToken: refreshToken,
                type_auth: 'vk',
                uid: profile.id,
                username: profile.displayName,
                photoUrl: profile.photos[0].value,
                profileUrl: profile.profileUrl,
                profile: profile
            });
        }
    ));
     
    passport.serializeUser(function (user, done) {
        done(null, user.id);
        console.log('serializeUser', user.id);
        //console.log('serializeUser', user);
        // _createOrUpdateUser(user, user.type_auth).then(function(_user){
        //         //console.log('_user', _user);
        //         done(null, _user.id);
        //     }).catch(function(err){
        //         //console.log('deserializeUser', err);
        //         done(err);    
        //     });
        //done(null, JSON.stringify(user));
    });
     
     
    passport.deserializeUser(function (user_id, done) {
        //console.log('deserializeUser', data);
        console.log('deserializeUser', user_id);
        _getUser(user_id).then(function(user){
                done(null, user);
            }).catch(function(err){
                done(err);
            });
        
    });    
    
};

//todo перенести куда-нибудь
/** авторизация */
//-------------------------

var _createOrUpdateUser = function(userData, auth_type){
    console.log('_createOrUpdateUser userData.loginedUserId', userData.loginedUser);
    var p = db.user_auth.find({ where: {uid: userData.uid, type_auth: auth_type} }).then(function(user_auth) {
        if(user_auth){
            //todo update userdata
            return db.user_auth.update({
                    token: userData.accessToken,
                    profile_data: JSON.stringify( userData.profile )
                }, {
                    where: { id : user_auth.id }
                })
                .then(function(){
                    return db.user.find({where: {id: user_auth.user_id} }).then(function(_user){
                        return _user;
                    });        
                });
            
        }else{

            var _save_user_auth = function(user_model){
                //console.log('user_model', user_model);
                
                return db.user_auth.create({
            
                    user_id: user_model.id,
                    type_auth: auth_type,
                    uid: userData.uid,
                    token: userData.accessToken,
                    profile_data: JSON.stringify( userData.profile )


                }).then(function(user_auth_model){
                    return user_model;
                });
            };

            //если идет привязка логина к существующему
            if(userData.loginedUser && userData.loginedUser.id){

                return _getUser(userData.loginedUser.id).then( _save_user_auth );

            }else{
                return db.user.create({
                    active: true
                }).then( _save_user_auth );
            }

            
        }
    });

   return p;
};

var _getUser = function(id){
  return db.user.find({ where: {id: id} }).then(function(_user) {
    return _user;
  });  
}