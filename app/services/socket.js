//todo use Redis
var Promise = require('promise');
var obj = {};

var storage = {};
storage.clients = {};
storage.coords = {};

obj.wait = function(coords, client){
	//todo validate coordinates
	return new Promise(function (resolve, reject) {
		if(!coords){
			return reject('set lat & lng');
		}
		storage.clients[client.id] = {
			coords: coords
		}
		console.log('storage.clients', storage.clients);
		return resolve();
	});
}

obj.unwait = function(client){
	return new Promise(function (resolve, reject) {
		if( storage.clients[client.id] ){
			delete(storage.clients[client.id]);
		}
		console.log('storage.clients', storage.clients);
		return resolve();
	});
	
}

obj.flashClient = function(client){
	obj.unwait(client);
}

module.exports = obj;