var mag = require('mag');

var logger = mag('app');

module.exports = {

	log: function(){
		logger.info.apply(null, arguments);
	},

	logger: logger
};