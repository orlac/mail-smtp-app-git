var express = require('express'),
  router = express.Router(),
  //marko = require('marko'),
  db = require('../models');
var passport = require('passport');  
var config = require('../../config/config')
var acl = require('../aclService');
var sendErrorService = require('../sendErrorService');

module.exports = function (app) {
  app.use('/auth', router);
};



router.get('/logout', function (req, res, next) {
    req.logout();
    res.redirect('/');
});



// router.post('/login', passport.authenticate('local', {
//         //successRedirect: '/cabinet',
//         //failureRedirect: '/',
//         failureFlash: 'Invalid username or password.',
//         successFlash: 'Welcome!'
//       })
//     );

// router.post('/login',
//   passport.authenticate('local'),
//   function(req, res) {
//     // If this function gets called, authentication was successful.
//     // `req.user` contains the authenticated user.
//     //res.redirect('/users/' + req.user.username);
//         res.send({
//           id: req.user,
//           go: '/cabinet'
//         });
//   });

router.post('/login', function(req, res, next) {
    

    passport.authenticate('local', 
      function(err, user, info) {
        console.log(err, user, info);

        if (err)  return sendErrorService.send(res, 403, err); 
        if (!user) return sendErrorService.send(res, 403, 'Пользователь не найден'); 

        req.logIn(user, function(err) {
          if (err) { return next(err); }
          res.send({
            id: user.id,
            go: '/cabinet'
          });
        });

        

    })(req, res, next);
});


