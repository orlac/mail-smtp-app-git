var express = require('express'),
  router = express.Router(),
  //marko = require('marko'),
  db = require('../models');
var config = require('../../config/config.js');  

module.exports = function (app) {
  app.use('/', router);
};

router.get('/', function (req, res, next) {
	console.log('config', config);
    res.render('index', {
    	config: config,
      	title: '',
      	date: new Date().toUTCString()
    });
});
