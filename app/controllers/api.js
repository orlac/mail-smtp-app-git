var express = require('express'),
  router = express.Router(),
  acl = require('../aclService'),
  db = require('../models');
var config = require('../../config/config');  

 

var sendErrorService = require('../sendErrorService');

module.exports = function (app) {
  app.use('/api/', router);
};

router.get('/map', function (req, res, next) {

    res.send({
		center: [ 42.1031271688029, 50.84571456032383],
		zoom: 9
    });
});


router.get('/user/get', function(req, res, next){
      	if(req.query.tz_i){
        	acl.ensurePreLoginAccess(req, res, next);
      	}else{
        	acl.ensureAccess(req, res, next)  
      	}
      
	}, function (req, res, next) {

		db.user.find({ where: {id: req.user.id} }).then(function(_user) {
			res.send({
		      		user: _user
		    	});
		}).catch(function(err){
	 		sendErrorService.send(res, 500, err);
		});
  
});