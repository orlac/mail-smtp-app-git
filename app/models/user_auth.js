var bcrypt = require('bcryptjs');
var Promise = require('promise');


module.exports = function (sequelize, DataTypes) {

  var model = sequelize.define('user_auth', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER,
            unique: 'user_id_type_auth'
        },
        createdAt: {
            type: DataTypes.DATE
        },
        updatedAt: {
            type: DataTypes.DATE
        },
        type_auth:  { 
            type : DataTypes.ENUM, 
            values : [ 'vk' , 'facebook' , 'pw' ],
            unique: 'user_id_type_auth' 
        },
        uid:  { 
            type : DataTypes.TEXT
        },
        token:  { 
            type : DataTypes.TEXT
        },
        username:  { 
                type : DataTypes.TEXT,
                validate: {
                    notEmpty:{
                        msg: "от 3 до 20 символов"
                    },
                    len: [3, 20],
                }
        },
        password:  { 
                type : DataTypes.TEXT,
                validate: {
                    notEmpty:{
                        msg: "от 6 до 20 символов"
                    },
                    len: [6, 20],
                }
        },
        profile_data:  { 
            type : DataTypes.TEXT
        }
    
    }, {

        getterMethods: {
            profileObject: function(){
                try{
                    return JSON.parse(this.profile_data);
                }catch(e){
                    return {};
                }
            }
        },
        classMethods: {
            associate: function (models) {
            // example on how to add relations
            // Article.hasMany(models.Comments);
            },
            getUserServiceByUid: function(user_id, type){
            
                return model.find({ where: {user_id: user_id, type_auth: type} });
            }
        },

        instanceMethods: {
            comparePassword: function(pw){
                var self = this;
                return new Promise(function(resolve, reject){
                    bcrypt.compare(pw, self.password, function(err, isMatch) {
                        if(err) return reject(err);
                        if(!isMatch) return reject('неверный пароль');
                        resolve(isMatch);
                    });    
                });
                
            },
            getProfileUrl: function(){
                switch(this.type_auth){
                    case 'vk':
                        return  this.profileObject.profileUrl;
                    break;
                    case 'facebook':
                        return  this.profileObject.profileUrl;
                    break;
                }
            },

            getAvatar: function(){
                switch(this.type_auth){
                    case 'vk':
                        if( Object.keys( this.profileObject.photos ).length > 0 ){
                            return this.profileObject.photos[0].value;
                        }
                    break;
                    case 'facebook':
                        return  this.profileObject.photo;
                    break;
                }
            },

            getUserName: function(){
                switch(this.type_auth){
                    case 'vk':
                        return  this.profileObject.displayName;
                    break;
                    case 'facebook':
                        return  this.profileObject.displayName;
                    case 'pw':
                        return  this.username;
                    break;
                }
            }
        }
  });

  return model;
};