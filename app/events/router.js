
var socketService = require('../services/socket');
var sendErrorService = require('../sendErrorService');



var fn = function(client){

	var _onError = function(err){
		client.emit('error', {error: sendErrorService.getErrObject(err) });
	}


	client.on('wait', function (data) { 
        
        //todo use any service, join coordinates and client.id
		console.log('wait', data);
		socketService.wait(data.coords, client).catch(_onError);

    });

    client.on('unwait', function (data) { 
        
        //todo use any service, join coordinates and client.id
        socketService.unwait(client).catch(_onError);
		console.log('unwait', data);
    });

    client.on('disconnect', function(){
        console.log('disconnect', client.id);
        socketService.flashClient(client);
    });
}

module.exports = {
	start: fn
}