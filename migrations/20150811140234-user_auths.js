"use strict";

var _tbl = 'user_auths';
module.exports = {
  up: function(migration, DataTypes, done) {
 //   var sql = "CREATE TABLE `user_auth` (`id` INT(11) NOT NULL AUTO_INCREMENT,`user_id` INT(11) NOT NULL,`createdAt` DATETIME NULL DEFAULT NULL,`updatedAt` DATETIME NULL DEFAULT NULL,`type_auth` ENUM('vk','fb','pw') NULL DEFAULT NULL,PRIMARY KEY (`id`),INDEX `FK_user_auth_user` (`user_id`),CONSTRAINT `FK_user_auth_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE ON DELETE CASCADE)ENGINE=InnoDB;";
 //   migration.query(sql).then(function(results, metadata) {
  //   done(null)
  // }, function(er){
  //  done(er)
  // });
  
    var _addIndex_0 = function(_tbl){
      return migration.addIndex(_tbl, ['user_id']);      
    }

    var _addIndex_1 = function(_tbl){
      return migration.addIndex(
        _tbl,
        ['user_id', 'type_auth'],
        {
          indexName: 'user_id_type_auth',
          indicesType: 'UNIQUE'
        }
      );
    }

    var res = migration.createTable(
        _tbl,
        {
          id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          user_id: {
            type: DataTypes.INTEGER
          },
          createdAt: {
            type: DataTypes.DATE
          },
          updatedAt: {
            type: DataTypes.DATE
          },
          type_auth:  { 
            type : DataTypes.ENUM, 
            values : [ 'vk' , 'facebook' , 'pw' ] 
          },
          uid:  { 
              type : DataTypes.TEXT
          },
          token:  { 
              type : DataTypes.TEXT
          },
          username:  { 
              type : DataTypes.TEXT
          },
          password:  { 
              type : DataTypes.TEXT
          },
          profile_data:  { 
              type : DataTypes.TEXT
          }
        }
      ).then(function(){
          return _addIndex_0(_tbl).then(function(){
            return _addIndex_1(_tbl);
          })    
      });
  
      res.then(function(){
        done();
      }).catch(done);
  
    //done();
  },

  down: function(migration, DataTypes, done) {
 //   var sql = "alter table user_auth drop foreign key FK_user_auth_user";
 //   migration.query(sql).then(function(results, metadata) {
  //   done(null)
  // }, function(er){
  //  done(er)
  // });
    return migration.dropTable(_tbl).then(function(){
        done();
    }).catch(done);
  }
};
