"use strict";


var _tbl = 'users';
module.exports = {
  up: function(migration, DataTypes, done) {

    migration.createTable(
          _tbl,
          {
            id: {
              type: DataTypes.INTEGER,
              primaryKey: true,
              autoIncrement: true
            },
            createdAt: {
              type: DataTypes.DATE
            },
            updatedAt: {
              type: DataTypes.DATE
            },
            role:  { 
              type : DataTypes.ENUM, 
              values : [ 'user' , 'admin' ] 
            },
            active: {
              type: DataTypes.BOOLEAN,
              defaultValue: true,
              allowNull: false
            }
          }
        ).then(function(){
            done();
        }).catch(done);  
    
    //done();
  },

  down: function(migration, DataTypes, done) {
    return migration.dropTable(_tbl).then(done).catch(done);;
    // add reverting commands here, calling 'done' when finished
    //done();
  }
};
